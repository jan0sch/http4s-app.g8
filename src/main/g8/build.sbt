// *****************************************************************************
// Build settings
// *****************************************************************************

addCommandAlias("check", "Compile/scalafix --check; Test/scalafix --check; scalafmtCheckAll")
addCommandAlias("fix", "Compile/scalafix; Test/scalafix; headerCreateAll; scalafmtSbt; scalafmtAll")

inThisBuild(
  Seq(
    scalaVersion := "$scala_version$",
    organization := "$organization$",
    organizationName := "$organization_name$",
    scalafmtOnCompile := false,
    scalacOptions ++= Seq(
        "-deprecation",
        "-explain",
        "-explain-types",
        "-feature",
        "-language:higherKinds",
        "-language:implicitConversions",
        "-no-indent",  // Prevent usage of indent based syntax.
        "-old-syntax", // Enforce classic syntax.
        "-unchecked",
        "-Wunused:imports",
        "-Wunused:linted",
        "-Wunused:locals",
        "-Wunused:nowarn",
        "-Wunused:params",
        "-Wunused:privates",
        "-Wunused:unsafe-warn-patvars",
        "-Wvalue-discard",
        //"-Xfatal-warnings", // FIXME: Make this work despite of Twirl!
        "-Ykind-projector",
    ),
    Compile / console / scalacOptions --= Seq("-Xfatal-warnings"),
    Test / console / scalacOptions --= Seq("-Xfatal-warnings"),
    Test / fork := true,
    Test / parallelExecution := false,
    Test / testOptions += Tests.Argument(TestFrameworks.MUnit, "-b")
  )
)

// *****************************************************************************
// Projects
// *****************************************************************************

lazy val website =
  project
    .in(file("."))
    .dependsOn(twirl)
    .enablePlugins(
      AutomateHeaderPlugin,
      BuildInfoPlugin,
      JavaServerAppPackaging,
      SbtTwirl
    )
    .settings(
      name := "$name$",
      buildInfoKeys := Seq[BuildInfoKey](name, version, scalaVersion, sbtVersion),
      buildInfoPackage := "$package$",
      startYear := Option($start_year$),
      licenses += ("MPL-2.0", url("https://www.mozilla.org/en-US/MPL/2.0/")),
      libraryDependencies ++= Seq(
        library.bouncyCastleProvider,
        library.catsCore,
        library.catsEffect,
        library.circeCore,
        library.circeGeneric,
        library.circeParser,
        library.doobieCore,
        library.doobieHikari,
        library.doobiePostgres,
        library.flywayCore,
        library.fs2Core,
        library.fs2IO,
        library.http4sCirce,
        library.http4sCore,
        library.http4sDsl,
        library.http4sEmberClient,
        library.http4sEmberServer,
        //library.http4sTwirl,
        library.ip4sCore,
        library.logback,
        library.postgresql,
        library.pureConfig,
        library.simpleJavaMail,
        library.springSecurityCrypto,
        library.sttpApiSpec,
        library.tapirCats,
        library.tapirCirce,
        library.tapirCore,
        library.tapirHttp4s,
        library.tapirOpenApiDocs,
        library.tapirSwaggerUi,
        library.munit           % Test,
        library.munitCatsEffect % Test,
        library.munitScalaCheck % Test,
        library.scalaCheck      % Test
      ),
      TwirlKeys.templateImports ++= Seq(
        "cats._",
        "cats.data._",
        "cats.syntax.all._",
        "org.http4s.Uri"
      )
    )

// FIXME: This is a workaround until http4s-twirl gets published properly for Scala 3!
lazy val twirl =
  project
    .in(file("twirl"))
    .enablePlugins(SbtTwirl)
    .settings(
      name := "twirl",
      libraryDependencies += library.http4sCore,
    )

// *****************************************************************************
// Library dependencies
// *****************************************************************************

lazy val library =
  new {
    object Version {
      val bouncyCastle    = "$bouncy_castle_version$"
      val cats            = "$cats_version$"
      val catsEffect      = "$cats_effect_version$"
      val circe           = "$circe_version$"
      val doobie          = "$doobie_version$"
      val flyway          = "$flyway_version$"
      val fs2             = "$fs2_version$"
      val http4s          = "$http4s_version$"
      val ip4s            = "$ip4s_version$"
      val logback         = "$logback_version$"
      val munit           = "$munit_version$"
      val munitCatsEffect = "$munit_cats_effect_version$"
      val postgresql      = "$postgresql_version$"
      val pureConfig      = "$pureconfig_version$"
      val scalaCheck      = "$scalacheck_version$"
      val simpleJavaMail  = "$simple_java_mail_version$"
      val springSecurity  = "$spring_security_version$"
      val sttpApiSpec     = "$sttp_apispec_version$"
      val tapir           = "$tapir_version$"
    }
    val bouncyCastleProvider       = "org.bouncycastle"              %  "bcprov-jdk15to18"        % Version.bouncyCastle
    val catsCore                   = "org.typelevel"                 %% "cats-core"               % Version.cats
    val catsEffect                 = "org.typelevel"                 %% "cats-effect"             % Version.catsEffect
    val circeCore                  = "io.circe"                      %% "circe-core"              % Version.circe
    val circeGeneric               = "io.circe"                      %% "circe-generic"           % Version.circe
    val circeParser                = "io.circe"                      %% "circe-parser"            % Version.circe
    val doobieCore                 = "org.tpolecat"                  %% "doobie-core"             % Version.doobie
    val doobieHikari               = "org.tpolecat"                  %% "doobie-hikari"           % Version.doobie
    val doobiePostgres             = "org.tpolecat"                  %% "doobie-postgres"         % Version.doobie
    val flywayCore                 = "org.flywaydb"                  %  "flyway-core"             % Version.flyway
    val fs2Core                    = "co.fs2"                        %% "fs2-core"                % Version.fs2
    val fs2IO                      = "co.fs2"                        %% "fs2-io"                  % Version.fs2
    val http4sCirce                = "org.http4s"                    %% "http4s-circe"            % Version.http4s
    val http4sCore                 = "org.http4s"                    %% "http4s-core"             % Version.http4s
    val http4sDsl                  = "org.http4s"                    %% "http4s-dsl"              % Version.http4s
    val http4sEmberServer          = "org.http4s"                    %% "http4s-ember-server"     % Version.http4s
    val http4sEmberClient          = "org.http4s"                    %% "http4s-ember-client"     % Version.http4s
    //val http4sTwirl                = "org.http4s"                    %% "http4s-twirl"            % Version.http4s
    val ip4sCore                   = "com.comcast"                   %% "ip4s-core"               % Version.ip4s
    val logback                    = "ch.qos.logback"                %  "logback-classic"         % Version.logback
    val munit                      = "org.scalameta"                 %% "munit"                   % Version.munit
    val munitCatsEffect            = "org.typelevel"                 %% "munit-cats-effect"       % Version.munitCatsEffect
    val munitScalaCheck            = "org.scalameta"                 %% "munit-scalacheck"        % Version.munit
    val postgresql                 = "org.postgresql"                %  "postgresql"              % Version.postgresql
    val pureConfig                 = "com.github.pureconfig"         %% "pureconfig-core"         % Version.pureConfig
    val scalaCheck                 = "org.scalacheck"                %% "scalacheck"              % Version.scalaCheck
    val simpleJavaMail             = "org.simplejavamail"            %  "simple-java-mail"        % Version.simpleJavaMail
    val springSecurityCrypto       = "org.springframework.security"  %  "spring-security-crypto"  % Version.springSecurity
    val sttpApiSpec                = "com.softwaremill.sttp.apispec" %% "openapi-circe-yaml"      % Version.sttpApiSpec
    val tapirCats                  = "com.softwaremill.sttp.tapir"   %% "tapir-cats"              % Version.tapir
    val tapirCirce                 = "com.softwaremill.sttp.tapir"   %% "tapir-json-circe"        % Version.tapir
    val tapirCore                  = "com.softwaremill.sttp.tapir"   %% "tapir-core"              % Version.tapir
    val tapirHttp4s                = "com.softwaremill.sttp.tapir"   %% "tapir-http4s-server"     % Version.tapir
    val tapirOpenApiDocs           = "com.softwaremill.sttp.tapir"   %% "tapir-openapi-docs"      % Version.tapir
    val tapirSwaggerUi             = "com.softwaremill.sttp.tapir"   %% "tapir-swagger-ui-bundle" % Version.tapir
  }
