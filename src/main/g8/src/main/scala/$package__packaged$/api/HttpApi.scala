package $package$.api

import cats._
import cats.effect._
import cats.syntax.all._
import io.circe._
import io.circe.generic.semiauto._
import org.http4s._
import org.http4s.dsl._
import org.slf4j.LoggerFactory
import sttp.model.StatusCode
import sttp.tapir._
import sttp.tapir.json.circe._
import sttp.tapir.server.http4s.Http4sServerInterpreter

/** An example HTTP API implemented via tapir endpoints and http4s.
  *
  * @tparam F
  *   A higher kinded type providing needed functionality, which is usually an IO monad like Async or Sync.
  */
final class HttpApi[F[_]: Async] extends Http4sDsl[F] {
  private final val log = LoggerFactory.getLogger(getClass)

  /** An example function to authenticate a request against a hard coded token.
    *
    * FIXME: You do not want to use this in prodution!
    *
    * @param bearerToken
    *   The bearer token passed with the request.
    * @return
    *   Either a string if the authentication was successful or a status code in the error case.
    */
  private def authenticate(bearerToken: String): F[Either[StatusCode, String]] =
    if (bearerToken === "FIXME") {
      Sync[F].delay(log.error("FIXME: You are using the default authentication for the API!")) *> Sync[F].delay(
        "Successfully authenticated!".asRight
      )
    } else
      Sync[F].delay(StatusCode.Forbidden.asLeft)

  private val echo: HttpRoutes[F] = Http4sServerInterpreter[F]().toRoutes(
    HttpApi.echo
      .serverSecurityLogic(authenticate)
      .serverLogic(authenticateOutput =>
        echoRequest => Sync[F].delay(EchoReply(s"Hello \${echoRequest.name}!").asRight[StatusCode])
      )
  )

  val routes = echo
}

/** Wrapper for the response of the echo endpoint.
  *
  * @param message
  *   The message that shall be returned.
  */
final case class EchoReply(message: String)

object EchoReply {
  given Decoder[EchoReply] = deriveDecoder[EchoReply]
  given Encoder[EchoReply] = deriveEncoder[EchoReply]
  given Schema[EchoReply]  = Schema.derived[EchoReply]
}

/** Wrapper for the input for the echo endpoint.
  *
  * @param name
  *   A name that shall be used in the reply.
  */
final case class EchoRequest(name: String)

object EchoRequest {
  given Decoder[EchoRequest] = deriveDecoder[EchoRequest]
  given Encoder[EchoRequest] = deriveEncoder[EchoRequest]
  given Schema[EchoRequest]  = Schema.derived[EchoRequest]
}

object HttpApi {
  val echo = endpoint.post
    .in("echo")
    .in(jsonBody[EchoRequest].example(EchoRequest("Bob")))
    .securityIn(auth.bearer[String]())
    .out(jsonBody[EchoReply].example(EchoReply("Hello Bob!")))
    .errorOut(statusCode)
    .description("A simple endpoint which will return a message based upon the given input.")
}
